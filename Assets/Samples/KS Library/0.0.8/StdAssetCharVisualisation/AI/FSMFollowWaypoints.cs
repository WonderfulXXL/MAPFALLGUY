﻿using KS.AI.FSM;
using KS.Character;
using UnityEngine;
using UnityEngine.AI;

namespace StdAssetCharVisualisation.AI
{
    [RequireComponent(typeof(UnityEngine.Animator))]
    [RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof(AICharacterControlKSModified))]
    public class FSMFollowWaypoints : FiniteStateMachine
    {
        public AIFollowWaypointsSettings aiSettings;
        
        public GameObject Npc { get; set; }
        public Animator Anim { get; set; }
        
        
        [SerializeField] public Transform player;
        public Transform Player
        {
            get { return player; }
        }
        
        
        public NavMeshAgent Agent { get; set; }
        public ThirdPersonCharacterKSModified ThirdPersonChar { get; set; }
        public AICharacterControlKSModified AICharControlKsModified { get; set; }
        
        private void Start()
        {
            //The start state
            CurrentState = new Idle(this);
            
            Npc = GetComponent<Transform>().gameObject;
            
            Anim = GetComponent<Animator>();
            Agent = GetComponent<NavMeshAgent>();
            
            ThirdPersonChar = GetComponent<ThirdPersonCharacterKSModified>();
            AICharControlKsModified = GetComponent<AICharacterControlKSModified>();
        }
    }
}
