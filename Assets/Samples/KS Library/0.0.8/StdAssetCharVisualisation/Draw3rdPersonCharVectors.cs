﻿using KS.Character;
using UnityEngine;

namespace StdAssetCharVisualisation
{
    [RequireComponent(typeof(ThirdPersonCharacterKSModifiedForPhysics))]
    [RequireComponent(typeof(ThirdPersonUserControlKSModifiedForPhysics))]
    public class Draw3rdPersonCharVectors : MonoBehaviour
    {
        Transform transform;
        private float vectorInfoLenght = 1.5f;

        private ThirdPersonCharacterKSModifiedForPhysics thirdPersonChar;
        private ThirdPersonUserControlKSModifiedForPhysics thirdPersonCharUserControl;
    
        // Start is called before the first frame update
        void Start()
        {
            transform = GetComponent<Transform>();
            thirdPersonChar = GetComponent<ThirdPersonCharacterKSModifiedForPhysics>();
            thirdPersonCharUserControl = GetComponent<ThirdPersonUserControlKSModifiedForPhysics>();
        }

        // Update is called once per frame
        void Update()
        {
            Debug.DrawLine(
                transform.position ,
                transform.position + (transform.forward)*vectorInfoLenght,
                Color.blue);
        
            Debug.DrawLine(
                transform.position ,
                transform.position + (transform.right)*vectorInfoLenght,
                Color.red);   
        
            Debug.DrawLine(
                transform.position ,
                transform.position + (transform.up)*vectorInfoLenght,
                Color.green);   
        
        
            Debug.DrawLine(
                transform.position ,
                transform.position + thirdPersonCharUserControl.MoveVector*thirdPersonChar.ForwardAmount,
                Color.yellow);   
        
        }
    }
}
